#export CSV_FOLDER="./test"
export CSV_FOLDER="/Users/jmatos/Downloads/prod_11may_3pmCEST"
export RAM=24000
export SPLIT_RATIO=8
export EXPORT_DATE="2020-05-11"
#export PROJECT_DEBUG=0000
#export GROUP_DEBUG=0000

#node --max-old-space-size=$RAM filter_independent.js

#in case of PROJECT_DEBUG, only run partition 0
#node --max-old-space-size=$RAM recompute.js 0

#TODO HIGH execute in parallel (but be careful with RAM)
for (( p=0; p<$SPLIT_RATIO; p++ ))
do
  node --max-old-space-size=$RAM recompute.js $p
done
