const memberMod = require('./member_light');
const sharedgroup = require('./shared_group');
const PROJECT_DEBUG = process.env.PROJECT_DEBUG;

function Project(parent, id) {
  this.parent = parent;
  this.id = id;
  this.members = [];
  this.sharedWith = [];

  this.addMember = function(memberId, role) {
    var member = new memberMod.MemberLight(memberId, true, role);
    this.members.push(member);
  }

  this.removeMember = function(memberId) {
    for(m in this.members) {
      if(this.members[m].id == memberId) {
        this.members.splice(m, 1);
        return;
      }
    }
  }

  this.computeProjectAuthorizations = function(computedProjectAuthorizations) {
    return processProjectAuthorizations(this.listMembers(), this.id, computedProjectAuthorizations);
  }

  this.shareWithGroup = function(group, maxRole) {
    //TODO implement access control

    //Confirmed by db analysis: share_with_group_lock is set in db in all subgroups
    if(this.parent.shareGroupLock == true) { //check if parent group has share lock feature
      return;
    }

    var sharedGroup = new sharedgroup.SharedGroup(group, maxRole);
    this.sharedWith.push(sharedGroup);
  }

  this.unshareWithGroup = function(group) {
    for(s in this.sharedWith) {
      if(this.sharedWith[s].group == group) {
        this.sharedWith.splice(s, 1);
        return;
      }
    }
  }

  this.listMembers = function() {
    if(this.id == PROJECT_DEBUG) {
      console.log("Direct members are "+JSON.stringify(this.members));
    }
    //TODO MEDIUM this could be done in parallel
    var inheritedCopied = [];
    var inheritedMembers = this.parent.listMembers();
    for(m in inheritedMembers) {
      var tempMember = new memberMod.MemberLight(inheritedMembers[m].id, false, inheritedMembers[m].role);
      if(this.id == PROJECT_DEBUG) {
        console.log("Inherited member from " +this.parent.id + " is "+ JSON.stringify(tempMember));
      }
      inheritedCopied.push(tempMember); //deep copy needed not to alter original object
    }
    var results = this.members.concat(inheritedCopied);

    var sharedMembers = [];
    for(s in this.sharedWith) {
      if(this.id == PROJECT_DEBUG) {
        console.log("This project is shared with group "+this.sharedWith[s].group.id);
      }
      var maxRole = this.sharedWith[s].maxRole;
      var tempMembers = this.sharedWith[s].group.listMembers();
      for(t in tempMembers) { //compute effective role
        var effectiveRole = Math.min(tempMembers[t].role, maxRole);
        var tempMember = new memberMod.MemberLight(tempMembers[t].id, tempMembers[t].direct, effectiveRole);
        if(this.id == PROJECT_DEBUG) {
          console.log("Shared  member from group " + this.sharedWith[s].group.id + " is "+ JSON.stringify(tempMember));
        }
        sharedMembers.push(tempMember);
      }
    }
    return results.concat(sharedMembers);
  }
}

function processProjectAuthorizations(members, projectId, computedProjectAuthorizations) {

  for(m in members) {
    var name = projectId + "/" +  members[m].id;
    var accessLevel = members[m].role;
    if(computedProjectAuthorizations.has(name)) {
      var currentAccess = computedProjectAuthorizations.get(name);
      if(accessLevel > currentAccess) {
        computedProjectAuthorizations.set(name, accessLevel);
      }
    }
    else {
      computedProjectAuthorizations.set(name, accessLevel);
    }
  }
}

module.exports = {Project};
