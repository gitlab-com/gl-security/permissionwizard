export CSV_FOLDER="."
export RAM=12000
export MODE="test_data_generation"

node --max-old-space-size=$RAM generate_fake_data.js

cd ..
./recompute.sh
cd test
