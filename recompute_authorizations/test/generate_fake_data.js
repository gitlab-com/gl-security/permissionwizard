const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const nbUsers = 2*1000*10;
//assumption 1 namepsace per user + a limited amount of groups estimated by this ratio
const groupVsNamepsaceRatio = 0.3;
//TODO also consider subgroups ratio

const projectsPerUser = 2;
const membersPerProject = 2;

generateUsers();

function generateUsers() {
  var users = [];
  for(n = 1; n <= nbUsers; n++) {
    var value = {id: n};
    users.push(value);
  }

  const usersCsv = createCsvWriter({
      path: 'users.csv',
      header: [
              {id: 'id', title: 'id'}
          ]
  });

  usersCsv.writeRecords(users)
      .then(() => {
          console.log('Users generated');
          generateGroups();
      });
  users = [];
}

function generateGroups() {
  const groupsCsv = createCsvWriter({
      path: 'sorted_namespaces.csv',
      header: [
              {id: 'id', title: 'id'},
              {id: 'type', title: 'type'},
              {id: 'parent_id', title: 'parent_id'},
          ]
  });

  var namespaces = [];
  for(n = 1; n <= nbUsers; n++) {
    var value = {id: n, type: '', parent_id: ''};
    namespaces.push(value);
  }

  const nbGroups = nbUsers * groupVsNamepsaceRatio;
  for(n = 1; n <= nbGroups; n++) {
    var value = {id: nbUsers+n, type: 'Group', parent_id: ''};
    namespaces.push(value);
  }

  groupsCsv.writeRecords(namespaces)
      .then(() => {
          console.log('Sorted namespaces generated');
          generateProjects();
      });

  namespaces = [];
}

function generateProjects() {
  const projectsCsv = createCsvWriter({
      path: 'projects.csv',
      header: [
              {id: 'id', title: 'id'},
              {id: 'namespace_id', title: 'namespace_id'},
              {id: 'creator_id', title: 'creator_id'},
          ]
  });

  var index = 0;
  var projects = [];
  for(n = 1; n <= nbUsers; n++) {
    for(p = 1; p <= projectsPerUser; p++) {
      index++;
      var value = {id: index, namespace_id: n, creator_id: n};
      projects.push(value);
    }
  }

  const nbGroups = nbUsers * groupVsNamepsaceRatio;
  for(n = 1; n <= nbGroups; n++) {
    for(p = 1; p <= projectsPerUser; p++) {
      index++;
      var value = {id: index, namespace_id: nbUsers+n, creator_id: n};
      projects.push(value);
    }
  }

  projectsCsv.writeRecords(projects)
      .then(() => {
          console.log('Projects generated');
          generateMembers();
      });
  projects = [];
}

function generateMembers() {
  const membersCsv = createCsvWriter({
      path: 'members.csv',
      header: [
              {id: 'id', title: 'id'},
              {id: 'user_id', title: 'user_id'},
              {id: 'source_id', title: 'source_id'},
              {id: 'type', title: 'type'},
              {id: 'source_type', title: 'source_type'},
              {id: 'access_level', title: 'access_level'},
              {id: 'expires_at', title: 'expires_at'},
              {id: 'ldap', title: 'ldap'}
          ]
  });

  var index = 0;
  var members = [];
  for(n = 1; n <= nbUsers; n++) {
    for(p = 1; p <= projectsPerUser; p++) {
      for(m = 1; m <= membersPerProject; m++) {
        index++;
        var user_id = (index % nbUsers) +1;
        var project_id = (n-1)*projectsPerUser + p;
        var role = (m % 5) + 1;
        var accessLevel = 10*role;
        var value = {id: index, user_id: user_id, source_id: project_id, type: 'ProjectMember', source_type: 'Project', access_level: accessLevel, expires_at: '', ldap: 'f'};
        members.push(value);
      }
    }
  }

  const nbGroups = nbUsers * groupVsNamepsaceRatio;
  for(n = 1; n <= nbGroups; n++) {
    for(m = 1; m <= membersPerProject; m++) {
      index++;
      var user_id = (index % nbUsers) +1;
      var group_id = n+nbUsers;
      var role = (m % 5) + 1;
      var accessLevel = 10*role;
      var value = {id: index, user_id: user_id, source_id: group_id, type: 'GroupMember', source_type: 'Namespace', access_level : accessLevel, expires_at: '', ldap: 'f'};
      members.push(value);
    }
  }

  membersCsv.writeRecords(members)
      .then(() => {
          console.log('Members generated');
          generateSharedGroups();
      });

  members = [];
}

function generateSharedGroups() {
  const sharedGroupsCsv = createCsvWriter({
      path: 'group_group_links.csv',
      header: [
              {id: 'id', title: 'id'},
              {id: 'shared_group_id', title: 'shared_group_id'},
              {id: 'shared_with_group_id', title: 'shared_with_group_id'},
              {id: 'expires_at', title:'expires_at'},
              {id: 'group_access', title: 'group_access'}
          ]
  });

  var sharedGroups = [];

  sharedGroupsCsv.writeRecords(sharedGroups)
      .then(() => {
          console.log('Shared groupds generated');
          generateSharedProjects();
      });
}

function generateSharedProjects() {
  const sharedProjectsCsv = createCsvWriter({
      path: 'project_groups_links.csv',
      header: [
              {id: 'id', title: 'id'},
              {id: 'project_id', title: 'project_id'},
              {id: 'group_id', title: 'group_id'},
              {id: 'expires_at', title:'expires_at'},
              {id: 'group_access', title: 'group_access'}
          ]
  });

  var sharedProjects = [];

  sharedProjectsCsv.writeRecords(sharedProjects)
      .then(() => {
          console.log('Shared projects generated');
      });
}
