//TODO HIGH, massively refactor with existing ./recompute.js
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const csv = require('csv-parser');
const fs = require('fs');
const path = require('path');
const date = require('date-and-time');
const member = require('../member_light');
const sharedgroup = require('../shared_group');
const group = require('../group');
const project = require('../project');

var dataFolder = process.env.CSV_FOLDER + "/";

var userIds = new Set();
var groupIds = new Set();
var namespaceIds = new Set(); //user namespaces that can contain projects, but not linked to a group
var projectIds = new Set();
var invalidMembersMissingProject = new Set();
var invalidMembersMissingGroup = new Set();
var invalidMembersUnknownUser = new Set();
var invalidProjectsMissingParent = new Set();
var invalidGroupsMissingParent = new Set();
var invalidSharedGroups = new Set();
var invalidSharedProjects = new Set();
var ignoredProjectAuthorizations = new Set();

var computedProjectAuthorizations = new Map();
var precomputedProjectAuthorizations = new Map();
var onlyPrecomputed = new Set();
var inconsistentAuthorizationValues = new Map();
var inconsistentUsers = new Set();
var projectCreators = new Map();
var sameAuthorizations = 0;

//TODO LOW: implement LDAP, only in tracing mode now
//TODO MEDIUM:  confirm there is no impact that group.js and project.js can only handle private projects
//TODO LOW: what about GMA accounts ? (group managed)

//start reading database dump files
var startTime = new Date();
var root = new group.Group("toplevel", 0);
//readAndSortCSV('csv_staging/namespaces.csv', cleanupNamespaces); //only needed once to sort and filter namespaces by id
readUsers();

function readUsers() {
  readCSVandProcess(dataFolder + 'users.csv', processUser, readGroups);
}

function readGroups() {
  readCSVandProcess(dataFolder + 'sorted_namespaces.csv', processGroup, readProjects);
}

function readProjects() {
  readCSVandProcess(dataFolder + 'dependent_projects.csv', processProject, readMembers);
}

function readMembers() {
  readCSVandProcess(dataFolder + 'dependent_members.csv', processMember, readSharedGroups);
}

function readSharedGroups() {
  readCSVandProcess(dataFolder + 'group_group_links.csv', processSharedGroup, readSharedProjects);
}

function readSharedProjects() {
  readCSVandProcess(dataFolder + 'project_groups_links.csv', processSharedProject, computeAuthorizations);
}

function readProjectAuthorizations() {
  readCSVandProcess(dataFolder + 'project_authorizations.csv', processProjectAuthorization, computeStats);
}

function computeGroupName(group_id) {
  return "group" + group_id;
}

function computeProjectName(project_id) {
  return "project" + project_id;
}

function computeParentGroupName(parent_group_id) {
  if(parent_group_id> 0) {
      return computeGroupName(parent_group_id);
  } else {
    return "root";
  }
}
function processUser(candidate) {
  userIds.add(Number(candidate.id));
}

function processGroup(candidate) {
  if(candidate.type == "Group") {
    if ((candidate.parent_id > 0) && !groupIds.has(Number(candidate.parent_id))) {
      invalidGroupsMissingParent.add(candidate);
      return;
    }

    groupIds.add(Number(candidate.id)); //optimization, path is not really needed
    var result = computeGroupName(candidate.id) + " = ";
    result += computeParentGroupName(candidate.parent_id);
    result += ".newSubgroup(" + candidate.id +");"
    eval(result);

  } else { //TODO Medium make sure the only other possible case is user namespace (e.g GMA ???)
    namespaceIds.add(Number(candidate.id)); //optimization, path is not really needed
    //a namespace can be considered a project directly linked to "root"
  }
}

function processProject(candidate) {
  var parentId = Number(candidate.namespace_id);
  var candidateId = Number(candidate.id);

  if (!groupIds.has(parentId) && !namespaceIds.has(Number(parentId))) {
    invalidProjectsMissingParent.add(candidate);
    return;
  }

  projectIds.add(candidateId); //optimization, path is not really needed
  var parentName;
  if(namespaceIds.has(parentId)) { //this is a user namespace
    parentName = "root"; //a namespace can be considered a project directly linked to "root"
  } else { //otherwise this is a group namespace
    parentName = computeGroupName(parentId);
  }

  var result = computeProjectName(candidateId) + " = " + parentName + ".newProject(" + candidateId +");"
  eval(result);

  var creatorId = Number(candidate.creator_id);
  if(creatorId > 0) {
    //projectCreators.set(candidateId, creatorId); //debug only

    if(parentName == "root") { //user namespace, we are sure to be the Maintainer
      var result = computeProjectName(candidate.id) + ".addMember(" + creatorId + "," + 40 + ");" //hard coded Maintainer
      eval(result);
    }
    //for groups this is (supposed to be) handled by the members table
  }

}

function isExpired(candidate) {
  if(candidate.expires_at != "") {
    var expiryDate = date.parse(candidate.expires_at, "YYYY-MM-DD");
    var diffInDays = date.subtract(expiryDate, startTime).toDays();
    return (diffInDays < 1);
  }
  return false;
}

function processMember(candidate) {
  var userId = Number(candidate.user_id);
  if(!userIds.has(userId)) {
    invalidMembersUnknownUser.add(candidate);
    return;
  }

  if(isExpired(candidate)) {
    return; //no need to process an expired member
  }

  if(candidate.ldap != 'f') {
    console.log(userId + " ldap is  "+candidate.ldap); //only debugging right now
  }

  var sourceId = Number(candidate.source_id);

  if ((candidate.type == "GroupMember") && (candidate.source_type == "Namespace")) {
    if(!groupIds.has(sourceId)) { //unknown member
      invalidMembersMissingGroup.add(candidate);
      return;
    }

    var groupName = computeGroupName(sourceId);
    result = groupName + ".addMember(" + userId + "," + candidate.access_level + ");"
    eval(result);
  }

  if ((candidate.type == "ProjectMember") && (candidate.source_type == "Project")) {
    if(!projectIds.has(sourceId)) { //unknown project
      invalidMembersMissingProject.add(candidate);
      return;
    }

    var projectName = computeProjectName(sourceId);
    result = projectName + ".addMember(" + userId + "," + candidate.access_level + ");"
    eval(result);
  }
}

function processSharedGroup(candidate) {
  if (candidate.id > 0) { //ignore empty lines
  } else {
    return;
  }

  if(isExpired(candidate)) {
    return; //no need to process an expired share
  }

  if(!groupIds.has(Number(candidate.shared_group_id)) ||
     !groupIds.has(Number(candidate.shared_with_group_id))) {
    invalidSharedGroups.add(candidate);
    return;
  }

  var result = computeGroupName(candidate.shared_group_id) + ".shareWithGroup(";
  result += computeGroupName(candidate.shared_with_group_id) + "," + candidate.group_access + ");";
  eval(result);
}

function processSharedProject(candidate) {
  if (candidate.id > 0) { //ignore empty lines
  } else {
    return;
  }

  if(isExpired(candidate)) {
    return; //no need to process an expired share
  }

  if(!groupIds.has(Number(candidate.group_id)) ||
     !projectIds.has(Number(candidate.project_id))) {
    invalidSharedProjects.add(candidate);
    return;
  }

  var result = computeProjectName(candidate.project_id) + ".shareWithGroup(";
  result += computeGroupName(candidate.group_id) + "," + candidate.group_access + ");";
  eval(result);
}

function computeAuthorizations() {
  var startDate = new Date();
  console.log("Starting computing authorizations at " + startDate.toGMTString());

  root.computeProjectAuthorizations(computedProjectAuthorizations);
  var duration = (Date.now() - startDate) / 1000;
  console.log("Finished computing authozations in " + duration + " seconds");

  console.log(computedProjectAuthorizations.size + " computed authorizations");
  console.log("Invalid groups (missing parent) number is "+ invalidGroupsMissingParent.size);
  console.log("Invalid projects (missing parent) number is "+ invalidProjectsMissingParent.size);
  console.log("Invalid members (unknown user) number is "+ invalidMembersUnknownUser.size);
  console.log("Invalid members (missing project) number is "+ invalidMembersMissingProject.size);
  console.log("Invalid members (missing group) number is "+ invalidMembersMissingGroup.size);

  //readProjectAuthorizations();
  const authorizationsCsv = createCsvWriter({
      path: 'project_authorizations.csv',
      header: [
              {id: 'user_id', title: 'user_id'},
              {id: 'project_id', title: 'project_id'},
              {id: 'access_level', title: 'access_level'}
          ]
  });

  var results = [];
  for (let [name, accessLevel] of computedProjectAuthorizations) {
    var userid = extractUserid(name);
    var projectid = extractProjectid(name);
    var value = {user_id: userid, project_id: projectid, access_level: accessLevel};
    results.push(value);
  }

  authorizationsCsv.writeRecords(results)
      .then(() => {
          console.log('Project authorizations generated');
      });

  results = [];
}


function processProjectAuthorization(candidate) {
  //if it is not a valid project_id or user_id, we could ignore it
  if(!projectIds.has(Number(candidate.project_id))) {
    ignoredProjectAuthorizations.add(candidate);
    return;
  }
  if(!userIds.has(Number(candidate.user_id))) {
    ignoredProjectAuthorizations.add(candidate);
    return;
  }

  var name = candidate.project_id + "/" + candidate.user_id;
  var accessLevel = Number(candidate.access_level);
  //precomputedProjectAuthorizations.set(name, accessLevel);

  if(computedProjectAuthorizations.has(name)) { //computed assocation for userid-projectid exists
    var computedLevel = computedProjectAuthorizations.get(name);
    if(computedLevel != accessLevel) { //permission level mismatch
      inconsistentAuthorizationValues.set(name, computedLevel + " vs " +accessLevel);
      inconsistentUsers.add(extractUserid(name));
    } else { //optimization: same entry both in precomputed and computed, we can clean it and only keep a counter for stats
      computedProjectAuthorizations.delete(name);
      sameAuthorizations +=1 ;
    }
  } else { //unknown computed userid-projectid association
    onlyPrecomputed.add(name);
  }

  //now computedProjectAuthorizations contains only the computedAuthorizations not in onlyPrecomputed, i.e. onlyComputed
}

function computeStats() {
  console.log(sameAuthorizations + " matching authorizations");

  console.log("Ignored "+ignoredProjectAuthorizations.size + " precomputed authorizations");
  console.log("Only in precomputed authorizations: " + onlyPrecomputed.size);
  console.log("Only in freshly computed authorizations: " + computedProjectAuthorizations.size);

  console.log(inconsistentAuthorizationValues.size + " inconsistent permission level entries");
  console.log(inconsistentUsers.size + " corresponding inconsistent users");

  var totalErrors = computedProjectAuthorizations.size + onlyPrecomputed.size + ignoredProjectAuthorizations.size + inconsistentAuthorizationValues.size;
  var errorRate = 100 * totalErrors/sameAuthorizations;
  console.log("Pessimistic global error/success rate " + errorRate.toFixed(2) + "%");

  console.log("Computing all inconsistent users");
  var ghostUsers = new Set();

  for (let [name, accessLevel] of computedProjectAuthorizations) {
    var userid = extractUserid(name);
    if(userIds.has(userid)) {
      inconsistentUsers.add(userid);
    } else {
      ghostUsers.add(userid);
    }
  }

  onlyPrecomputed.forEach(function(candidate) {
    var userid = extractUserid(candidate);
    if(userIds.has(userid)) {
      inconsistentUsers.add(userid);
    } else {
      ghostUsers.add(userid);
    }

  });

  ignoredProjectAuthorizations.forEach(function(candidate) {
    var userid = Number(candidate.user_id);
    if(userIds.has(userid)) {
      inconsistentUsers.add(userid);
    } else {
      ghostUsers.add(userid);
    }
  });

  saveSet('inconsistentUsers.txt', inconsistentUsers);
  saveSet('ghostUsers.txt', ghostUsers);
  console.log("There are " + inconsistentUsers.size + " total inconsistent users, i.e. " +
      (100*(inconsistentUsers.size/userIds.size)).toFixed(3) + "% of total users");
  console.log("There are " + ghostUsers.size + " ghost users, i.e. referenced users not in USERS table");

  duration = (Date.now() - startTime) / (60*1000);
  console.log("Finished overall processing in " + duration.toFixed(1) + " minutes");

}

function checkProjectCreators() { //TODO LOW: should not be needed anymore (debug feature)
  for (let [projectId, creatorId] of projectCreators) {
    var name = projectId + "/" + creatorId;
    if(!computedProjectAuthorizations.has(name) && precomputedProjectAuthorizations.has(name)) {
      console.log("Creator "+creatorId+" of project "+projectId+" not in computed authorizations but in precomputed");
    }
    if(!precomputedProjectAuthorizations.has(name) && computedProjectAuthorizations.has(name)) {
      console.log("Creator "+creatorId+" of project "+projectId+" not in precomputed authorizations but in computed");
    }
  }
}

function extractUserid(name) {
  var pos = name.indexOf("/");
  return Number(name.substring(pos+1));
}

function extractProjectid(name) {
  var pos = name.indexOf("/");
  return Number(name.substring(0,pos));
}

function saveSet(filename, values) {
  var file = fs.createWriteStream(filename);
  file.on('error', function(err) { });

  values.forEach(function(value) {
    file.write(value +'\n');
  })
  file.end();
}

function cleanupNamespaces(values) {
  var file = fs.createWriteStream(dataFolder + 'sorted_namespaces.csv',  {flags: 'w' });
  file.on('error', function(err) { });

  file.write("id,path,owner_id,type,parent_id"+ '\n');
  values.forEach(function(value) {
    file.write(value.id + ',' + value.path + ',' + value.owner_id + ',' + value.type + ','+  value.parent_id + '\n');
  })
  file.end();
}

function readAndSortCSV(filename, callback) {
  var results =[];
  var startDate = new Date();
  console.log("Starting reading " + filename + " at " + startDate.toGMTString());
  fs.createReadStream(filename)
    .pipe(csv({ separator: ',' }))
    .on('data', (row) => {
      results.push(row);
    })
    .on('end', () => {
      var sortedResults = results.sort(function(a, b) {
        return a.id - b.id;
      });

      var duration = (Date.now() - startDate) / 1000;
      console.log("Finished reading and sorting " + filename + " in " + duration + " seconds");
      results = [];
      callback(sortedResults);
    });
}

function readCSVandProcess(filename, processor, callback) {
  //assumption results are already sorted or order does not matter
  var results =[];
  var startDate = new Date();
  console.log("Starting reading and processing " + filename + " at " + startDate.toGMTString());
  fs.createReadStream(filename)
    .pipe(csv({ separator: ',' }))
    .on('data', (row) => {
      processor(row);
    })
    .on('end', () => {
      var duration = (Date.now() - startDate) / 1000;
      console.log("Finished processing " + filename + " in " + duration + " seconds");
      callback();
    });
}
