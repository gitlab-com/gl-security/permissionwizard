const csv = require('csv-parser');
const fs = require('fs');
const path = require('path');
const date = require('date-and-time');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const member = require('./member_light');
const sharedgroup = require('./shared_group');
const group = require('./group');
const project = require('./project');

const dataFolder = process.env.CSV_FOLDER + "/";
const SPLIT_RATIO = process.env.SPLIT_RATIO;
const PROJECT_DEBUG = process.env.PROJECT_DEBUG;
const GROUP_DEBUG = process.env.GROUP_DEBUG;
const EXPORT_DATE = process.env.EXPORT_DATE;
const MAX_DEPTH = 22;
const myArgs = process.argv.slice(2);
const partition = myArgs[0];

var groupIds = new Set();
var namespaceIds = new Set(); //user namespaces that can contain projects, but not linked to a group
var projectIds = new Set();

var computedProjectAuthorizations = new Map();
var precomputedProjectAuthorizations = new Map();
var onlyPrecomputed = new Map();
var inconsistentAuthorizationValues = new Map();
var sameAuthorizations = 0;

const sharedProjectHeader = "id,project_id,group_id,group_access,expires_at";
var invalid_shared_projects_missing_project = openCSVFile('invalid_shared_projects_missing_project_'+partition+'.csv', sharedProjectHeader);
var invalid_shared_projects_missing_group = openCSVFile('invalid_shared_projects_missing_group_'+partition+'.csv', sharedProjectHeader);
const sharedGroupHeader = "id,shared_group_id,shared_with_group_id,expires_at,group_access";
var invalid_shared_groups_missing_group = openCSVFile('invalid_shared_groups_missing_group_'+partition+'.csv', sharedGroupHeader);

//TODO LOW: implement LDAP, only in tracing mode now
//TODO MEDIUM:  confirm there is no impact that group.js and project.js can only handle private projects
//TODO LOW: what about GMA accounts ? (group managed)

//TODO MEDIUM: confirm created_by id in Member is not needed in the logic, like creator_id for project

var startTime = new Date();
var baseTime = startTime;
if(EXPORT_DATE != "") {
  baseTime = date.parse(EXPORT_DATE, "YYYY-MM-DD");
}
var root = new group.Group("toplevel", 0);

//start reading database dump files
readNamespaces();

var depth = 0;
function readNamespaces() {
    readCSVandProcess(dataFolder + 'independent_namespaces.csv', processNamespace, readGroups);
}

function readGroups() {
  if(depth < MAX_DEPTH) {
    depth = depth +1;
    readCSVandProcess(dataFolder + 'dependent_groups_'+depth+'.csv', processGroup, readGroups);
  } else {
    readCSVandProcess(dataFolder + 'dependent_groups_'+depth+'.csv', processGroup, readSharedGroups);
  }
}

function readSharedGroups() {
  depth = 0;
  readCSVandProcess(dataFolder + 'group_group_links.csv', processSharedGroup, readDependentGroupMembers);
}

function readDependentGroupMembers() {
  if(depth < MAX_DEPTH) {
    depth = depth +1;
    readCSVandProcess(dataFolder + 'dependent_group_members_'+depth+'.csv', processMember, readDependentGroupMembers);
  } else {
    readCSVandProcess(dataFolder + 'dependent_group_members_'+depth+'.csv', processMember, readProjects);
  }

}

function readProjects() {
  readCSVandProcess(dataFolder + 'dependent_projects_'+partition+'.csv', processProject, readMembers);
}

function readMembers() {
  readCSVandProcess(dataFolder + 'dependent_members_'+partition+'.csv', processMember, readSharedProjects);
}

function readSharedProjects() {
  readCSVandProcess(dataFolder + 'project_groups_links.csv', processSharedProject, computeAuthorizations);
}

function readProjectAuthorizations() {
  readCSVandProcess(dataFolder + 'splitted_authorizations_'+partition+'.csv', processProjectAuthorization, computeStats);
}

function isEmpty(candidate) {
  if (candidate.id > 0) {
    return false;
  } else {
    return true;
  }
}

function computeGroupName(group_id) {
  return "group" + group_id;
}

function computeProjectName(project_id) {
  return "project" + project_id;
}

function computeParentGroupName(parent_group_id) {
  if(parent_group_id> 0) {
      return computeGroupName(parent_group_id);
  } else {
    return "root";
  }
}

function processNamespace(candidate) {
    //a namespace can be considered a project directly linked to "root"
  namespaceIds.add(Number(candidate.id));
}

function processGroup(candidate) {
  var candidateId = Number(candidate.id);
  groupIds.add(candidateId); //optimization, path is not really needed

  var result = computeGroupName(candidate.id) + " = ";
  result += computeParentGroupName(candidate.parent_id);
  result += ".newSubgroup(" + candidate.id +");"
  eval(result);

  if(candidate.share_with_group_lock == "t") {
    var result = computeGroupName(candidate.id) + ".lockShareGroup();"
    eval(result);
  }
}

function processProject(candidate) {
  var parentId = Number(candidate.namespace_id);
  var candidateId = Number(candidate.id);

  projectIds.add(candidateId); //optimization, path is not really needed
  var parentName;
  if(namespaceIds.has(parentId)) { //this is a user namespace
    parentName = "root"; //a namespace can be considered a project directly linked to "root"
  } else { //otherwise this is a group namespace
    parentName = computeGroupName(parentId);
  }

  var result = computeProjectName(candidate.id) + " = " + parentName + ".newProject(" + candidate.id +");"
  eval(result);

  var ownerId = Number(candidate.owner_id);
  if(parentName == "root") { //user namespace, we are sure to be the Maintainer
    var result = computeProjectName(candidate.id) + ".addMember(" + ownerId + "," + 40 + ");" //hard coded Maintainer
    eval(result);
  }
  //for groups this is handled by the members table
}

function isExpired(candidate) {
  if(candidate.expires_at != "") {
    var expiryDate = date.parse(candidate.expires_at, "YYYY-MM-DD");
    var diffInDays = date.subtract(expiryDate, baseTime).toDays();
    return (diffInDays < 0);
  }
  return false;
}

function processMember(candidate) {
  var userId = Number(candidate.user_id);
  if(isExpired(candidate)) {
    return; //no need to process an expired member
  }

  if(candidate.ldap != 'f') {
    console.log(userId + " ldap is  "+candidate.ldap); //only debugging right now
  }

  var sourceId = Number(candidate.source_id);

  if ((candidate.type == "GroupMember") && (candidate.source_type == "Namespace")) {
    if(sourceId == GROUP_DEBUG) {
      console.log("This group " + sourceId + " has member "+ userId);
    }
    var groupName = computeGroupName(sourceId);
    result = groupName + ".addMember(" + userId + "," + candidate.access_level + ");"
    eval(result);
  }

  if ((candidate.type == "ProjectMember") && (candidate.source_type == "Project")) {
    var projectName = computeProjectName(sourceId);
    result = projectName + ".addMember(" + userId + "," + candidate.access_level + ");"
    eval(result);
  }
}

function processSharedGroup(candidate) {

  if(isEmpty(candidate)) {
    return;
  }

  var invalidResult = "\n" + candidate.id + "," + candidate.shared_group_id + "," + candidate.shared_with_group_id + "," +
                candidate.expires_at + "," + candidate.group_access;
  if(!groupIds.has(Number(candidate.shared_group_id)) || !groupIds.has(Number(candidate.shared_with_group_id))) {
    invalid_shared_groups_missing_group.write(invalidResult);
    return;
  }

  if(isExpired(candidate)) {
    return; //no need to process an expired share
  }

  var result = computeGroupName(candidate.shared_group_id) + ".shareWithGroup(";
  result += computeGroupName(candidate.shared_with_group_id) + "," + candidate.group_access + ");";
  eval(result);
}

function processSharedProject(candidate) {
  if(isEmpty(candidate)) {
    return;
  }

  var project_id = Number(candidate.project_id);
  var group_id = Number(candidate.group_id);
  var invalidResult = "\n" + candidate.id + "," + candidate.project_id + "," + candidate.group_id + "," +
                candidate.group_access + "," + candidate.expires_at;

  if(!groupIds.has(group_id)) {
    invalid_shared_projects_missing_group.write(invalidResult);
    return;
  }

  if(split_function(project_id) != partition) {
    return; //this shared project is in another partition
  }
  if(!projectIds.has(project_id)) {
    invalid_shared_projects_missing_project.write(invalidResult);
    return;
  }

  if(isExpired(candidate)) {
    return; //no need to process an expired share
  }

  var result = computeProjectName(candidate.project_id) + ".shareWithGroup(";
  result += computeGroupName(candidate.group_id) + "," + candidate.group_access + ");";
  eval(result);
}

function computeAuthorizations() {
  var startDate = new Date();
  console.log("Starting final computing authorizations at " + startDate.toGMTString());

  root.computeProjectAuthorizations(computedProjectAuthorizations);
  var duration = (Date.now() - startDate) / 1000;
  console.log("Finished computing authorizations in " + duration + " seconds");

  console.log(computedProjectAuthorizations.size + " total computed authorizations");

  if(process.env.MODE == "test_data_generation") {
     exportInvalidAuthorizations(computedProjectAuthorizations, 'splitted_authorizations_' + partition + '.csv')
  } else {
    readProjectAuthorizations();
  }
}

function processProjectAuthorization(candidate) {
  var name = candidate.project_id + "/" + candidate.user_id;
  var accessLevel = Number(candidate.access_level);

  if(computedProjectAuthorizations.has(name)) { //computed assocation for userid-projectid exists
    var computedLevel = computedProjectAuthorizations.get(name);
    //we can delete the association and then just keep stats about it
    computedProjectAuthorizations.delete(name);

    if(computedLevel != accessLevel) { //permission level mismatch
      inconsistentAuthorizationValues.set(name, computedLevel + " vs precomputed " +accessLevel);
    } else { //same entry both in precomputed and computed, we can clean it and only keep a counter for stats
      sameAuthorizations +=1 ;
    }

  } else { //unknown computed userid-projectid association
    onlyPrecomputed.set(name, accessLevel);
  }

  //now computedProjectAuthorizations contains only the computedAuthorizations not in onlyPrecomputed, i.e. onlyComputed
}

function computeStats() {
  console.log(sameAuthorizations + " matching authorizations");
  displayMap(onlyPrecomputed,"Only in precomputed authorizations");
  displayMap(computedProjectAuthorizations,"Only in freshly computed authorizations");
  displayMap(inconsistentAuthorizationValues,"Inconsistent authorizations");

  var totalErrors = computedProjectAuthorizations.size + onlyPrecomputed.size + inconsistentAuthorizationValues.size;
  var errorRate = 100 * totalErrors/(totalErrors+sameAuthorizations);
  console.log("Pessimistic global error/success rate " + errorRate.toFixed(3) + "%");

  saveInvalidAuthorizations();

  duration = (Date.now() - startTime) / (60*1000);
  console.log("Finished partition " + partition + " processing in " + duration.toFixed(1) + " minutes");
}

function displayMap(data, prefix) {
    console.log(prefix+ ": " + data.size);
    if(PROJECT_DEBUG > 0) {
          console.log(prefix+ ": " + JSON.stringify(Array.from(data.entries())));
    }
}

function extractUserid(name) {
  var pos = name.indexOf("/");
  return Number(name.substring(pos+1));
}

function extractProjectid(name) {
  var pos = name.indexOf("/");
  return Number(name.substring(0,pos));
}

function split_function(project_id) {
  return project_id % SPLIT_RATIO;
}

function saveSet(filename, values) {
  var file = fs.createWriteStream(filename);
  file.on('error', function(err) { });

  values.forEach(function(value) {
    file.write(value +'\n');
  })
  file.end();
}

function saveInvalidAuthorizations() {
  exportInvalidAuthorizations(computedProjectAuthorizations, 'only_in_computed_authorizations_' + partition + '.csv');
  exportInvalidAuthorizations(onlyPrecomputed, 'only_in_precomputed_authorizations_' + partition + '.csv');
  exportInvalidAuthorizations(inconsistentAuthorizationValues, 'inconsistent_authorization_values_' + partition + '.csv');
  //TODO LOW wait for this async process to finish before computing end time (even though those files should be small)
}

function exportInvalidAuthorizations(data, filename) {
  const authorizationsCsv = createCsvWriter({
      path: dataFolder + filename,
      header: [
              {id: 'user_id', title: 'user_id'},
              {id: 'project_id', title: 'project_id'},
              {id: 'access_level', title: 'access_level'}
          ]
  });

  var results = [];
  for (let [name, accessLevel] of data) {
    var userid = extractUserid(name);
    var projectid = extractProjectid(name);
    var value = {user_id: userid, project_id: projectid, access_level: accessLevel};
    results.push(value);
  }

  authorizationsCsv.writeRecords(results)
      .then(() => {
          console.log(filename + " exported");
      });

  results = [];
}

function readCSVandProcess(filename, processor, callback) {
  //assumption results are already sorted or order does not matter
  var results =[];
  var startDate = new Date();
  console.log("Starting reading and processing " + filename + " at " + startDate.toGMTString());
  fs.createReadStream(filename)
    .pipe(csv({ separator: ',' }))
    .on('data', (row) => {
      processor(row);
    })
    .on('end', () => {
      var duration = (Date.now() - startDate) / 1000;
      console.log("Finished processing " + filename + " in " + duration + " seconds");
      callback();
    });
}

function openCSVFile(filename, header) {
  fs.writeFile(dataFolder+filename, '', function(){})
  var result = fs.createWriteStream(dataFolder+filename, {
    flags: 'a'
  })
  result.write(header);
  return result;
}
