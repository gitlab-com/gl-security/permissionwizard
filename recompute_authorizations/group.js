const memberMod = require('./member_light');
const sharedgroup = require('./shared_group');
const projectMod = require('./project');
const GROUP_DEBUG = process.env.GROUP_DEBUG;

const notAMember = 5; //cf roles

function Group(parent, id) {
  this.parent = parent;
  this.id = id;
  this.projects = [];
  this.subgroups = [];
  this.members = [];
  this.sharedWith = [];
  this.shareGroupLock = false;

  this.lockShareGroup = function() {
    this.shareGroupLock = true;
  }

  this.computeProjectAuthorizations = function(computedProjectAuthorizations) {
    //we dont need the group members to compute the project authorizations, only the project members
    for(p in this.projects) {
      var proj = this.projects[p];
      proj.computeProjectAuthorizations(computedProjectAuthorizations);
    }

    for(s in this.subgroups) {
      var subgroup = this.subgroups[s];
      subgroup.computeProjectAuthorizations(computedProjectAuthorizations);
    }
  }

  this.addMember = function(memberId, role) {
    var member = new memberMod.MemberLight(memberId, true, role);
    this.members.push(member);
  }

  this.removeMember = function(memberId) {
    for(m in this.members) {
      if(this.members[m].name == memberId) {
        this.members.splice(m, 1);
        return;
      }
    }
  }

  this.listMembers = function(sharing = true) {
      var indirectCopied = [];
      if(this.parent.id > 0) { //if there is a real parent group
        var indirectMembers = this.parent.listMembers(); //compute indirect members
        for(m in indirectMembers) {
          var tempMember = new memberMod.MemberLight(indirectMembers[m].id, false, indirectMembers[m].role);
          indirectCopied.push(tempMember); //deep copy needed not to alter original object
        }
      }
      var results = this.members.concat(indirectCopied); //add indirect members to direct members

      var sharedMembers = [];
      for(s in this.sharedWith) {
        if(this.sharedWith[s].group.id == GROUP_DEBUG) {
          console.log("This group "+ this.id +" has invited group "+this.sharedWith[s].group.id);
        }
        var found = findSharedMembers(this.sharedWith[s]);
        sharedMembers = sharedMembers.concat(found);
      }
      results = results.concat(sharedMembers);

      return results;
  }

  this.newProject = function(projectId) {
      var proj = new projectMod.Project(this, projectId);
      this.projects.push(proj);
      return proj;
  }

  this.newSubgroup = function(subgroupId) {
      var subgroup = new Group(this, subgroupId);
      this.subgroups.push(subgroup);
      return subgroup;
  }

  this.shareWithGroup = function(group, maxRole) {
    //TODO implement access control
    var sharedGroup = new sharedgroup.SharedGroup(group, maxRole);
    this.sharedWith.push(sharedGroup);
  }

  this.unshareWithGroup = function(group) {
    for(s in this.sharedWith) {
      if(this.sharedWith[s].group == group) {
        this.sharedWith.splice(s, 1);
        return;
      }
    }
  }
}

function findSharedMembers(sharedGroup) {
  var results = [];
  var tempMembers = sharedGroup.group.members; //we only need the direct group members
  if(sharedGroup.group.id==GROUP_DEBUG) {
    console.log("Found "+tempMembers.length+ " members in shared group "+GROUP_DEBUG);
  }
  for(t in tempMembers) { //compute effective role
    if(sharedGroup.group.id==GROUP_DEBUG) {
      console.log("Found shared member " +tempMembers[t].id+ " in shared group "+GROUP_DEBUG);
    }
    var effectiveRole = Math.min(tempMembers[t].role, sharedGroup.maxRole);
    var tempMember2 = new memberMod.MemberLight(tempMembers[t].id, true, effectiveRole);
    results.push(tempMember2);
  }
  return results;
}

module.exports = {Group};
