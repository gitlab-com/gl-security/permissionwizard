"use strict";

function MemberLight(id, direct, role) {
  this.id = id;
  this.direct = direct;
  this.role = role;
}

module.exports = {MemberLight};
