const csv = require('csv-parser');
const fs = require('fs');

const dataFolder = process.env.CSV_FOLDER + "/";
const SPLIT_RATIO = process.env.SPLIT_RATIO;
const PROJECT_DEBUG = process.env.PROJECT_DEBUG;
const GROUP_DEBUG = process.env.GROUP_DEBUG;
const MAX_DEPTH = 22;

var parents = new Map();
var namespaceIds = new Map(); //user namespaces that can contain projects, but not linked to a group
var groupIds = new Set();
var projectIds = new Set();

const projectHeader = "id,namespace_id,owner_id";
const memberHeader = "id,user_id,source_id,type,source_type,access_level,expires_at,ldap";
const authorizationHeader = "user_id,project_id,access_level";
const namespaceHeader = "id,owner_id,type,parent_id,share_with_group_lock";

var userIds = new Set();
var dependentGroups = [];
var dependentGroupMembers = [];
var dependentProjects = [];
var dependentMembers = [];
var splittedAuthorizations = [];

var independentNamespaces = openCSVFile('independent_namespaces.csv', namespaceHeader);

for(var d=0; d <= MAX_DEPTH; d++) {
  dependentGroups[d] = openCSVFile('dependent_groups_' + d + '.csv', namespaceHeader);
  dependentGroupMembers[d] = openCSVFile('dependent_group_members_' + d + '.csv', memberHeader);
}

for(var i=0; i < SPLIT_RATIO; i++) {
  dependentProjects[i] = openCSVFile('dependent_projects_' + i + '.csv', projectHeader);
  dependentMembers[i] = openCSVFile('dependent_members_' + i + '.csv', memberHeader);
  splittedAuthorizations[i] = openCSVFile('splitted_authorizations_' + i + '.csv', authorizationHeader);
}

var invalid_namespaces_missing_owner = openCSVFile('invalid_namespaces_missing_owner.csv', namespaceHeader);
var invalid_groups_missing_parent = openCSVFile('invalid_groups_missing_parent.csv', namespaceHeader);
var invalid_projects_missing_parent = openCSVFile('invalid_projects_missing_parent.csv', projectHeader);
var invalid_projects_missing_creator = openCSVFile('invalid_projects_missing_creator.csv', projectHeader);
var invalid_members_missing_user = openCSVFile('invalid_members_missing_user.csv', memberHeader);
var invalid_members_missing_project = openCSVFile('invalid_members_missing_project.csv', memberHeader);
var invalid_members_missing_group = openCSVFile('invalid_members_missing_group.csv', memberHeader);
var invalid_project_authorizations_missing_user = openCSVFile('invalid_project_authorizations_missing_user.csv', authorizationHeader);
var invalid_project_authorizations_missing_project = openCSVFile('invalid_project_authorizations_missing_project.csv', authorizationHeader);

readUsers();

function readUsers() {
  readCSVandProcess('users.csv', processUser, readNamespaces);
}

function readNamespaces() { //NB: sorted namepsaces is not needed anymore as we filter groups by depth
  readCSVandProcess('sorted_namespaces.csv', preProcessNamespace, readGroups);
}

function readGroups() {
  readCSVandProcess('sorted_namespaces.csv', processGroup, readProjects);
}

function readProjects() {
  readCSVandProcess('projects.csv', processProject, readMembers);
}

function readMembers() {
  readCSVandProcess('members.csv', processMember, splitProjectAuthorizations);
}

function splitProjectAuthorizations() {
  if (fs.existsSync(dataFolder + 'project_authorizations.csv')) {
    readCSVandProcess('project_authorizations.csv', splitProjectAuthorization, finish)
  } else {
    finish();
  }
}


function finish() {
  independentNamespaces.end();

  for(var d=0; d <= MAX_DEPTH; d++) {
    dependentGroups[d].end();
    dependentGroupMembers[d].end();

  }
  for(var i=0; i < SPLIT_RATIO; i++) {
    dependentProjects[i].end();
    dependentMembers[i].end();
    splittedAuthorizations[i].end();
  }

  invalid_namespaces_missing_owner.end();
  invalid_groups_missing_parent.end();
  invalid_projects_missing_parent.end();
  invalid_projects_missing_creator.end();
  invalid_members_missing_user.end();
  invalid_members_missing_project.end();
  invalid_members_missing_group.end();
  invalid_project_authorizations_missing_user.end();
  invalid_project_authorizations_missing_project.end();
}

function split_function(project_id) {
  return project_id % SPLIT_RATIO;
}

function processUser(candidate) {
  userIds.add(Number(candidate.id));
}

function isEmpty(candidate) {
  if (candidate.id > 0) {
    return false;
  } else {
    return true;
  }
}

function preProcessNamespace(candidate) {
  if(isEmpty(candidate)) {
    return;
  }

  var ownerId = Number(candidate.owner_id);
  if((ownerId >0) && !userIds.has(ownerId)) {
    var result = "\n" + candidate.id + "," + candidate.path + "," + candidate.owner_id + "," +
                candidate.type + "," + candidate.parent_id;
    invalid_namespaces_missing_owner.write(result);
    return;
  }

  var candidateId = Number(candidate.id);
  if(candidate.parent_id > 0) {
    parents.set(candidateId, Number(candidate.parent_id));
  } else {
    parents.set(candidateId, -1);
  }
}

function isParentMissing(parentId) {
  if (parentId == 0) {
    return false;
  }

  if(!parents.has(parentId)) {
    return true;
  }

  var p = parents.get(parentId);
  if(p == -1) {
    return false;
  } else {
    return isParentMissing(p);
  }
}

function getParentDepth(groupId) {
  if(!parents.has(groupId)) {
    return 0;
  }

  var p = parents.get(groupId);
  if(p == -1) {
    return 1;
  } else {
    return 1 + getParentDepth(p);
  }
}

function processGroup(candidate) {
  //TODO HIGH refactor with what is already done in preProcessNamespace, e.g. ownerId checks
  if(isEmpty(candidate)) {
    return;
  }

  var candidateId = Number(candidate.id);
  var parentId = Number(candidate.parent_id);
  var ownerId = Number(candidate.owner_id);
  var result = "\n" + candidate.id + "," + candidate.owner_id + "," +
              candidate.type + "," + candidate.parent_id + "," + candidate.share_with_group_lock;

  if((ownerId >0) && !userIds.has(ownerId)) {
    return;
  }
  //TODO VERY HIGH groups can also have a owner_id
  if(candidate.type == "Group") {
    if (isParentMissing(parentId)) {
      invalid_groups_missing_parent.write(result);
      return;
    }
    var depth = getParentDepth(candidateId);
    groupIds.add(candidateId); //needed later on for error handling
    dependentGroups[depth].write(result);

  } else {
    namespaceIds.set(candidateId, ownerId);
    independentNamespaces.write(result);
  }
}

function processProject(candidate) {
  if(isEmpty(candidate)) {
    return;
  }
  if(PROJECT_DEBUG > 0) {
    if(candidate.id != PROJECT_DEBUG) {
      return;
    }
  }

  var parentId = Number(candidate.namespace_id);
  var candidateId = Number(candidate.id);
  var result = "\n" + candidate.id + "," + candidate.namespace_id + ",";

  if(!groupIds.has(parentId) && !namespaceIds.has(parentId)) {
    invalid_projects_missing_parent.write(result);
    return;
  }
  var ownerId = namespaceIds.get(parentId);
  result += ownerId;

  var creatorId = Number(candidate.creator_id);
  if(!userIds.has(creatorId) || (creatorId == 0)) {
    invalid_projects_missing_creator.write(result);
    //return; //this should not be a permission concern as creatorId is used
  }

  projectIds.add(candidateId);
  var index = split_function(candidateId);
  dependentProjects[index].write(result);
}

function processMember(candidate) {
  if(isEmpty(candidate)) {
    return;
  }

  if(candidate.requested_at == "") {
  } else {
    if(candidate.invite_accepted_at == "") { //user has not joined yet
      return;
    }
  }

  //TODO HIGH we could get rid of expired candidates here

  var user_id = Number(candidate.user_id );
  var result = "\n" + candidate.id + "," + candidate.user_id + "," + candidate.source_id + "," +
                candidate.type + "," + candidate.source_type + "," + candidate.access_level + "," +
                candidate.expires_at + "," + "f"; //we are forcing candidate.ldap to `f` as it is the case for all entries in prod

  if(!userIds.has(user_id)) {
    invalid_members_missing_user.write(result);
    return;
  }

  if ((candidate.type == "ProjectMember") && (candidate.source_type == "Project") ) {
    var project_id = Number(candidate.source_id);
    if(PROJECT_DEBUG > 0) {
      if(project_id != PROJECT_DEBUG) {
        return;
      }
    }
    if(!projectIds.has(project_id)) {
      invalid_members_missing_project.write(result);
      return;
    }
    //project shared with a group is the same than being a project in a group:
    //groups are stored in common
    var index = split_function(project_id);
    dependentMembers[index].write(result);
    return;
  }

  if ((candidate.type == "GroupMember") && (candidate.source_type == "Namespace")) {
    var group_id = Number(candidate.source_id);
    if(group_id == GROUP_DEBUG) {
      console.log("This group "+ group_id +" has member "+user_id);
    }
    if(groupIds.has(group_id)) {
      var depth = getParentDepth(group_id);
      dependentGroupMembers[depth].write(result);
    } else {
      invalid_members_missing_group.write(result);
    }
    return;
  }

  //This should not happen
  console.log("SHOULD NOT HAPPEN "+result);
}

function splitProjectAuthorization(candidate) {
  var user_id = Number(candidate.user_id);
  var project_id = Number(candidate.project_id);
  var result = "\n" + candidate.user_id + "," + candidate.project_id + "," + candidate.access_level;

  if(PROJECT_DEBUG > 0) {
    if(project_id != PROJECT_DEBUG) {
      return;
    }
  }

  if(!userIds.has(user_id)) {
    invalid_project_authorizations_missing_user.write(result);
    return;
  }
  if(!projectIds.has(project_id)) {
    invalid_project_authorizations_missing_project.write(result);
    return;
  }

  var index = split_function(project_id);
  splittedAuthorizations[index].write(result);
}

function readCSVandProcess(filename, processor, callback) {
  //assumption results are already sorted or order does not matter
  var results =[];
  var startDate = new Date();
  console.log("Starting reading and processing " + filename + " at " + startDate.toGMTString());
  fs.createReadStream(dataFolder + filename)
    .pipe(csv({ separator: ',' }))
    .on('data', (row) => {
      processor(row);
    })
    .on('end', () => {
      var duration = (Date.now() - startDate) / 1000;
      console.log("Finished processing " + filename + " in " + duration + " seconds");
      callback();
    });

}

function openCSVFile(filename, header) {
  fs.writeFile(dataFolder+filename, '', function(){})
  var result = fs.createWriteStream(dataFolder+filename, {
    flags: 'a'
  })
  result.write(header);
  return result;
}
