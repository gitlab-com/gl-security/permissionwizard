const ROLES = {
  anonymous: 0, notAMember: 1, guest: 2, reporter: 3,
  developer: 4, maintainer: 5, owner:6
}

const ROLE_NAMES = ['Anonymous', 'Not a member', 'Guest', 'Reporter', 'Developer', 'Maintainer', 'Owner']

const USER_STATES = {
  activated: 'Activated', deactivated: 'Deactivated', blocked: 'Blocked',
  expired: 'Expired', locked: 'Locked'
}

function UserInformation(role, activationState, isExternal, isAuditor, isAdmin) {
  this.role = role;
  this.activationState = activationState;
  this.external = isExternal;
  this.auditor = isAuditor;
  this.admin = isAdmin;

  this.same = function(otherUser) {
    return (this.role == otherUser.role) && (this.activationState == otherUser.activationState) &&
      (this.external == otherUser.external) && (this.auditor == otherUser.auditor) && (this.admin == otherUser.admin);
  }
}

function provideDetailedUserInfo(userInformation) {
  if(userInformation.admin) {
    return "Admin not a member of this project";
  }
  if(userInformation.auditor) {
    return "Auditor not a member of this project";
  }

  var result = "";
  if(userInformation.external) {
    result = "External, ";
  }
  if(userInformation.activationState != USER_STATES.activated) {
    result = userInformation.activationState + ", " + result;
  }

  if (userInformation.role == ROLES.anonymous) {
    return result + "Not logged in";
  }

  return result + ROLE_NAMES[userInformation.role] + " of this project";
}

function Member(name, direct, role) {
  this.name = name;
  this.direct = direct;
  this.userInformation = new UserInformation(role, USER_STATES.activated, false, false, false);

  this.display = function() {
    inheritance = this.direct ? "(Direct/" : "(Indirect/";
    return "<b><i>" + this.name + "</i></b> " + inheritance + ROLE_NAMES[this.userInformation.role] + ")";
  }
}

function displayMembers(members) {
  results = "";
  for(m in members) {
    results += members[m].display() + ", ";
  }
  return results;
}
