
var currentFeature = 0;
var currentPriority = 0;
var tableHeaders = "<table class=\"table table-bordered table-striped\"><thead class=\"thead-dark\"><tr>" +
  "<th style='width: 10%'>Item</th>" +
  "<th style='width: 20%'>Project information</th>" +
  "<th style='width: 22%'>User information</th>" +
  "<th style='width: 6%'>UI View</th>" +
  "<th style='width: 6%'>UI Update</th>" +
  "<th style='width: 7%'>API GET</th>" +
  "<th style='width: 8%'>API POST/PUT</th>" +
  "<th style='width: 8%'>GraphQL query</th>" +
  "<th style='width: 9%'>GraphQL mutation</th>" +
  "<th style='width: 4%'>Priority</th>" +
  "</tr></thead>\n";

function initDisplay() {
  currentFeature = FEATURES.issues;
  currentPriority = 1; //P1
  generateTable(currentFeature, currentPriority);
}

function setFeature(feature) {
    currentFeature = feature;
    generateTable(currentFeature, currentPriority);
}

function setPriority(priority) {
    currentPriority = priority;
    generateTable(currentFeature, currentPriority);
}

function generateTable(feature, priority) {
  var title = "<h2>" + FEATURE_NAMES[feature] + " test cases</h2><p>\n";
  var footers = "</table>\n";
  var testCases = getTestCasesByPriority(priority, computeAllTestsCases(feature));
  var r = title + tableHeaders + displayTestCases(testCases) + footers;
  document.getElementById("results").innerHTML = r;
}

function displayTestCases(testCases) {
  result = "<tbody>";
  for(var t in testCases) {
    result = result + testCases[t].display() + "\n";
  }
  return result + "</tbody>";
}
