//implements the permission rules as described in using https://docs.gitlab.com/ee/user/permissions.html

function computeExpectations(feature, role, projectAccessLevel, activationState, isExternal) {
  ui_expectationRO = uiExpectation(false, feature, role, projectAccessLevel, activationState, isExternal);
  ui_expectationW = uiExpectation(true, feature, role, projectAccessLevel, activationState, isExternal);
  api_expectationRO = apiExpectation(false, feature, role, projectAccessLevel, activationState, isExternal);
  api_expectationW = apiExpectation(true, feature, role, projectAccessLevel, activationState, isExternal);

  return new Expectations(ui_expectationRO, ui_expectationW, api_expectationRO, api_expectationW);
}

function adminExpectations() {
  return new Expectations(ACCESS_STATUS.allowed, ACCESS_STATUS.allowed, ACCESS_STATUS.allowed, ACCESS_STATUS.allowed);
}

function auditorExpectations() {
  return new Expectations(ACCESS_STATUS.allowed, ACCESS_STATUS.forbidden, ACCESS_STATUS.allowed, ACCESS_STATUS.forbidden);
}

function uiExpectation(writeAccess,feature, role, projectAccessLevel, activationState, isExternal) {
  //Non activated user states only make sense for API calls as user cannot log in UI otherwise
  if(activationState != USER_STATES.activated) {
    return ACCESS_STATUS.na;
  }

  return sharedExpectation(writeAccess, feature, role, projectAccessLevel, activationState, isExternal);
}

function apiExpectation(writeAccess,feature, role, projectAccessLevel, activationState, isExternal) {
  if(role == ROLES.anonymous) {//a token is required for API calls
    return ACCESS_STATUS.na;
  }

  //Non activated user states only make sense for API calls as user cannot log in UI otherwise
  if(activationState != USER_STATES.activated) {
    return ACCESS_STATUS.forbidden;
  }

  return sharedExpectation(writeAccess, feature, role, projectAccessLevel, activationState, isExternal);
}

function sharedExpectation(writeAccess, feature, role, projectAccessLevel, activationState, isExternal) {
  if (projectAccessLevel == PROJECT_LEVELS.public) {
    return ACCESS_STATUS.allowed;
  }
  if (projectAccessLevel == PROJECT_LEVELS.publicDisabled) {
    return ACCESS_STATUS.forbidden;
  }

  if (projectAccessLevel == PROJECT_LEVELS.internal) {
    if(isExternal==false) { //for an internal user
      return (role > ROLES.anonymous) ? ACCESS_STATUS.allowed : ACCESS_STATUS.forbidden; //true if user is logged in (i.e. identity is known)
    }
    //in case of external user, it will be evaluated as if it was a private project
  }

  //now projectAccessLevel is either Private or PublicRestricted, or we have an external user, i.e. user mut be a member
  if (role <= ROLES.notAMember) {
    return ACCESS_STATUS.forbidden;
  }

  if(writeAccess) {
    return roleCanAccessWriteFeature(role, feature) ? ACCESS_STATUS.allowed : ACCESS_STATUS.forbidden;
    } else {
    return roleCanAccessReadFeature(role, feature) ? ACCESS_STATUS.allowed : ACCESS_STATUS.forbidden;
  }
}

function roleCanAccessReadFeature(role, feature) {
  switch(feature) {
    case FEATURES.issue: {
      return role >= ROLES.guest;
    }
    case FEATURES.confidential_issue: {
      return role >= ROLES.reporter;
    }
    case FEATURES.mr: {
      return role >= ROLES.reporter;
    }
    case FEATURES.milestone: { //Access to milestones requires both Issues and MR accesses
      return roleCanAccessReadFeature(role, FEATURES.issue) && roleCanAccessReadFeature(role, FEATURES.mr);
    }
    case FEATURES.code: {
      return role >= ROLES.guest;
    }
    case FEATURES.commit: {
      return role >= ROLES.guest;
    }
    case FEATURES.wiki: {
      return role >= ROLES.guest;
    }
    case FEATURES.snippet: {
      return role >= ROLES.guest; //Not explicitely documented (but the goal of snippets is to share them)
    }
    default: {
      return ACCESS_STATUS.na;
    }
  }
}

function roleCanAccessWriteFeature(role, feature) {
  switch(feature) {
    case FEATURES.issue: {
      return role >= ROLES.reporter;
    }
    case FEATURES.confidential_issue: {
      return role >= ROLES.reporter;
    }
    case FEATURES.mr: {
      return role >= ROLES.developer;
    }
    case FEATURES.milestone: { //Access to milestones requires both Issues and MR accesses
      return roleCanAccessWriteFeature(role, FEATURES.issue) && roleCanAccessWriteFeature(role, FEATURES.mr);
    }
    case FEATURES.code: {
      return role >= ROLES.developer;
    }
    case FEATURES.commit: {
      return role >= ROLES.developer;
    }
    case FEATURES.wiki: {
      return role >= ROLES.guest;
    }
    case FEATURES.snippet: {
      return role >= ROLES.reporter; //TODO review because this not really editing, and external users cannot create snippets
    }
    default: {
      return ACCESS_STATUS.na;
    }
  }
}
