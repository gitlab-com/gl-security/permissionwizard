// generates the test cases and computes their priority

function computeAllTestsCases(feature) {

  if(feature == FEATURES.issues) { //Issues can be regular or confidential
    var results = computeAllTestsCases(FEATURES.issue);
    results = results.concat(computeAllTestsCases(FEATURES.confidential_issue));
    return results;
  }

  if(feature == FEATURES.comment) {
    //Comments can be found in Issues, MRs or commits
    var results = computeAllTestsCases(FEATURES.issues);
    results = results.concat(computeAllTestsCases(FEATURES.mr));
    results = results.concat(computeAllTestsCases(FEATURES.commit));
    return results;
  }

  var testCases = [];
  feature = parseInt(feature);
  for(var l in PROJECT_LEVELS) {
    var level = PROJECT_LEVELS[l];
    computedTestCases = generateTestCasesForProjectLevel(feature, level);
    testCases = testCases.concat(computedTestCases);

    //auditor and admin test cases
    testCases.push(computeAuditorTestCase(feature, level));
    testCases.push(computeAdminTestCase(feature, level));
  }

  if(feature == FEATURES.milestone) { //this is an AND on Issues and MR
    //TODO handle with Issues Projects Members only and also Merge Requests as Project Members Only
    //OR is it really needed for negative permission testing ???
    //we somehow want the projectAccessLevel of Issue with the milestone predicate
    //extraTestCases = generateTestCasesForProjectLevel(features.ISSUE, "PublicRestricted");
    //testCases = testCases.concat(extraTestCases);
  }


  deprioritizeUselessTestCases(testCases);
  return testCases;
}

function generateTestCasesForProjectLevel(feature, projectLevel) {
  var testCases = [];
  for(var r in ROLES) {
    var role = ROLES[r];

    for(var s in USER_STATES) {
      var priority = decidePriorityFromProjectAccessLevel(projectLevel);
      var userState = USER_STATES[s];
      priority += decidePriorityForNonActivatedUser(userState);

      var userInfo = new UserInformation(role, userState, false, false, false);
      var expected = computeExpectations(feature, role, projectLevel, userState, false);
      var test1 = new TestCase(feature, projectLevel, userInfo, expected);
      test1.priority = priority;
      test1.priority += decidePriorityFromLowerRole(test1);

      //external user
      var externalUserInfo = new UserInformation(role, userState, true, false, false);
      var expected2 = computeExpectations(feature, role, projectLevel, userState, true);
      var test2 = new TestCase(feature, projectLevel, externalUserInfo, expected2);
      test2.priority = priority + decidePriorityForExternalUser(projectLevel); //external user is less prioritary, particularly for public projects
      test2.priority += decidePriorityFromLowerRole(test2);

      testCases.push(test1);
      testCases.push(test2);
    }
  }
  return testCases;
}

function computeAuditorTestCase(feature, projectLevel) {
  var auditorInfo = new UserInformation(ROLES.notAMember, USER_STATES.activated, false, true, false);
  var expectedAuditor = auditorExpectations();
  if(projectLevel == PROJECT_LEVELS.publicDisabled) {
    expectedAuditor = new Expectations(ACCESS_STATUS.forbidden, ACCESS_STATUS.forbidden, ACCESS_STATUS.forbidden, ACCESS_STATUS.forbidden);
  }

  var testAuditor = new TestCase(feature, projectLevel, auditorInfo, expectedAuditor);
  testAuditor.priority = 3;
  return testAuditor;
}

function computeAdminTestCase(feature, projectLevel) {
  var adminInfo = new UserInformation(ROLES.notAMember, USER_STATES.activated, false, false, true);
  var expectedAdmin = adminExpectations();
  if(projectLevel == PROJECT_LEVELS.publicDisabled) {
    expectedAdmin = new Expectations(ACCESS_STATUS.forbidden, ACCESS_STATUS.forbidden, ACCESS_STATUS.forbidden, ACCESS_STATUS.forbidden);
  }

  var testAdmin = new TestCase(feature, projectLevel, adminInfo, expectedAdmin);
  testAdmin.priority = 3;
  return testAdmin;
}
