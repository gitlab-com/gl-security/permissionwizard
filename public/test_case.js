function TestCase(feature, projectInfo, userInfo, expected) {
  this.feature = feature;
  this.projectInformation = projectInfo;
  this.userInformation = userInfo;
  this.expectations = expected;
  this.priority = 0; //to be computed

  this.display = function() {
    projectDetails = provideDetailedProjectInfo(this.feature, this.projectInformation);
    return "<tr><td>" + FEATURE_NAMES[this.feature] + "</td><td>" +
      projectDetails + "</td><td>" + provideDetailedUserInfo(this.userInformation) + "</td><td>" +
      this.expectations.uiRead +  "</td><td>" + this.expectations.uiWrite +
      "</td><td>" + this.expectations.apiRead + "</td><td>" + this.expectations.apiWrite +
      "</td><td>" + this.expectations.apiRead + "</td><td>" + this.expectations.apiWrite + //graphQL 
      "</td><td>" + this.priority + "</td><tr>\n";
  }

  this.same = function(otherTestCase) {
    return this.projectInformation == otherTestCase.projectInformation &&
      this.feature == otherTestCase.feature &&
      this.userInformation.same(otherTestCase.userInformation) &&
      this.expectations.same(otherTestCase.expectations);
      //priority ignored on purpose
  }

  this.sameThanLowerRole = function(otherUserInfo, otherExpectations) {
    return (this.userInformation.role > otherUserInfo.role) &&
      (this.userInformation.activationState == otherUserInfo.activationState) &&
      (this.userInformation.external == otherUserInfo.external) &&
      (this.userInformation.auditor == otherUserInfo.auditor) &&
      (this.userInformation.admin == otherUserInfo.admin) &&
      (this.expectations.same(otherExpectations)) &&
      (this.expectations.noforbidden());
  }
}

function getTestCasesByPriority(priority, testCases) {
  var results = [];
  for(var t in testCases) {
      if(testCases[t].priority == priority) {
        results.push(testCases[t]);
      }
    }
  return results;
}
